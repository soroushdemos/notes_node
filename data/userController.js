import FileStorage from '../helper/fileStorage.js'

const LOGIN_OBJECT = 'userLogin_object'
const USER_OBJECT_NAME = (email) => "user_".concat(email)
const FS = new FileStorage()

var loginData = {};

export default class UserController {
    constructor() {
        FS.loadObject(LOGIN_OBJECT, (err, data) => {
            if (err) console.log(`Error loading object ${LOGIN_OBJECT}: ${err}`)
            if (data) loginData = JSON.parse(data)
        })
    }

    checkUserName(email) {
        return loginData && email in loginData
    }

    checkPassword(email, password) {
        return loginData[email] === password
    }

    fetchUser(email, callback) {
        FS.loadObject(USER_OBJECT_NAME(email), callback)
    }

    createUser(email, password, user, callback) {
        console.log("creating user")
        loginData[email] = password;
        FS.saveObject(LOGIN_OBJECT, loginData, (err, data) => {
            if (err) {
                console.log(`Error saving ${LOGIN_OBJECT}: ${err}`)
                callback(err, data)
                return
            }
            var objectName = USER_OBJECT_NAME(email)
            FS.saveObject(objectName, user, (err, data) => {
                if (err) {
                    console.log(`Error saving ${objectName}: ${e}`)
                    callback(err, null)
                }
                else callback(null, user)
            })
        });
    }

    updateUser(email, user, callback) {
        console.log(`updating user ${email}`)
        FS.saveObject(USER_OBJECT_NAME(email), user, (err, data) => {
            if (err) {
                console.log(`Error saving ${USER_OBJECT_NAME(email)}: ${e}`)
                callback(err, null)
            }
            else callback(null, user)
        })
    }
}