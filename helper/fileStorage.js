import fs from 'fs';

const HOME = 'data/repo'

function getPathforObject(objectName) {
    return `${HOME}/${objectName}.json`
}

export default class FileStorage {
    constructor() {

    }

    loadObject(objectName, callback) {
        console.log(`reading file ${getPathforObject(objectName)}`)
        fs.readFile(getPathforObject(objectName), 'utf-8', callback)
    }

    saveObject(objectName, data, callback) {
        console.log(`writing file ${getPathforObject(objectName)}`)
        fs.writeFile(getPathforObject(objectName), JSON.stringify(data), callback)
    }
}