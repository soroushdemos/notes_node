import express from 'express'
import UserEndpoint from './routes/user.js'
import HomeEndpoint from './routes/home.js'
import bodyParser from 'body-parser'

startApp()

function startApp() {
    console.log("Hello World!")
    const app = express()
    const port = 3000

    // setup parsers
    app.use(express.json())
    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: true }))

    // setup endpoints
    setupEndpoints(app)

    // start listening
    app.listen(port, () => console.log(`YAY! Notes API listening on port ${port}!`))
}

function setupEndpoints(app) {
    [
        HomeEndpoint,
        UserEndpoint
    ]
    .forEach((route) => new route(app));
}


