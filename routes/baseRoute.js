export default class BaseRoute {
    constructor(app) {
        this.app = app
        if (!this.setupRoutes) throw ("Method `setupRoutes` not implemented!")
        this.setupRoutes()
    }
}