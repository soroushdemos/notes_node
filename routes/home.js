import BaseRoute from './baseRoute.js'

export default class HomeEndpoint extends BaseRoute {
    setupRoutes() {
        this.app.get('/', (req, res) => { 
            console.log("/ requested")
            res.send('Hello From Notes API!')
        })
    }
}