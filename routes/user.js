import BaseRoute from './baseRoute.js'
import UserController from '../data/userController.js'
import { resolveNaptr } from 'dns'

const _userController = new UserController()

export default class UserEndpoint extends BaseRoute {
    setupRoutes() {
        this.setupLogin()
        this.setupSignUp()
        this.setupUserInfo()
        this.setupUserUpdate()
    }

    setupLogin() {
        this.app.get('/user/login', (req, res) => {
            console.log(`GET /user/login params=${JSON.stringify(req.query)}`)
            if (req.query.username && req.query.password) {
                let userExists = _userController.checkUserName(req.query.username)
                let passwordIsCorrect = _userController.checkPassword(req.query.username, req.query.password)
                if (!userExists) {
                    res.status(404)
                    res.send(`User with email '${req.query.username}' does not exist`)
                    return
                }

                if (passwordIsCorrect) {
                    res.send("OK")
                    return
                }

                res.status(401)
                res.send("Password is incorrect!")
            } else {
                res.status(422)
                res.send("Query parameters `username` and `password` are required")
            }
        })
    }

    setupSignUp() {
        this.app.post('/user/:username', (req, res) => {
            console.log(`POST /user.login/${req.params.username} params=${JSON.stringify(req.query)} and body=${JSON.stringify(req.body)}`)
            if (req.params.username && req.query.password && req.body) {
                console.log(req.body)
                _userController.createUser(req.params.username, req.query.password, req.body, (err, data) => {
                    if (err) {
                        console.log(err)
                        res.status(500)
                        res.send(`Could not create use ${req.params.username}`)
                        return
                    }
                    res.send(data)
                })
            } else {
                res.status(422)
                res.send("Route parameter email in /user/email, query parameter `password` and request body required!")
            }
        })
    }

    setupUserInfo() {
        this.app.get('/user/:username', (req, res) => {
            console.log(`GET /user/login/${req.params.username}`)
            if (req.params.username) {
                var email = req.params.username
                _userController.fetchUser(email, (err, data) => {
                    if (err) {
                        res.status(404)
                        res.send(`User ${email} not found!`)
                        return
                    }
                    else {
                        console.log(`User '${email}: ${JSON.stringify(data)}'`)
                        res.send(data)
                    }
                })
            } else {
                res.status(422)
                res.send("Route parameter email in user/email required!")
            }
        })
    }

    setupUserUpdate() {
        this.app.put('/user/:username', (req, res) => {
            console.log(`PUT /user/${req.params.username} body=${JSON.stringify(req.body)}`)
            if (req.params.username && req.body) {
                _userController.updateUser(req.params.username, req.body, (err, data) => {
                    if (err) {
                        const errrorMessage = `Error updating user ${req.params.username}: ${err}`
                        console.log(errrorMessage)
                        res.status(500)
                        res.send(errrorMessage)
                    }
                    else res.send(data)
                })
            } else {
                res.status(422)
                res.send("Route parameter email in user/email and request body required!")
            }
        })
    }
}